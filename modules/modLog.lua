local log = {}
local function contentFormat(event,logType)
  local e = tostring(event)
  local lT = tostring(logType)
  local output = '[' .. os.date('At: %I:%M:%S') .. '][' .. lT .. '][' .. e .. ']\n'
  return output
end
function log.write(event,logType)
  local filename = tostring('log_' .. os.date('%Y_%m_%d') .. '.txt')
  local file = io.open(filename,"a+")
  file:write(contentFormat(event,logType))
  file:close()
end
return log
