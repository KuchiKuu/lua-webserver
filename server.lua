#!/usr/bin/lua
local socket = require "socket"
local log = require "modules/modLog"
local args = require "modules/argParser"
local serverHostname = "localhost"
local serverPortNumber = 8080

argument = arg
args.paramRecieve(argument)

local isParamVerbose = args.isVerbose()
print(isParamVerbose)

local isParamLogging = args.isLogging()
local isParamDebug = args.isDebug()
local rootFolder = "www/"
local serverIndexFilename = "index.html"
local LOGGING_INFO = "INFO"
local LOGGING_WARNING = "WARNING"
local LOGGING_ERROR = "ERROR"

function Verbose(stringToVerbose,verboseType)
	if(isParamVerbose) then
		print(stringToVerbose)
	end
	if(isParamLogging) then
		log.write(stringToVerbose,verboseType)
	end

end

function Interpret(pageContent)
	local isLuaCodeFound = false
	local lua_code = "" -- this is where the output from the interpreter is saved
	local page_Output = "" -- this is where the HTML is saved line by line

	for line in pageContent:gmatch("(.-)\n") do -- for loop to get each line
		if(line == "<%LUA") then -- This must be in a new line to begin the interpreter
			isLuaCodeFound = true
			Verbose("-> Interpreter: Lua start",LOGGING_INFO)
			goto INTERPRETforEND -- skip everything below
		end
		if(line == "LUA%>") then -- this must be in a new line to end the interpreter AND ONLY THIS
			Verbose("-> Interpreter: Lua stop",LOGGING_INFO)
			isLuaCodeFound = false
			-- in order to print text into the page content
			-- a new function called echo() is created
			-- as print() sends text to the standard output
			function echo(s)
				page_Output = page_Output.."\n"..s
			end
			load(lua_code)() --executes saved lua code from string
			lua_code = "" 	-- resets the code, as there can be multiple fragments of lua. 
							-- To not duplicate code.
			goto INTERPRETforEND -- skip everything below
		end
		if isLuaCodeFound then -- if lua code is found
			lua_code = lua_code .. line .. "\n" -- add code to the lua_code to be executed later
			goto INTERPRETforEND
		end
		page_Output = page_Output .. line -- just add html to the page
		::INTERPRETforEND::
	end
	return page_Output
end
-- checking for passed arguments
-- -v   - verbose
-- -p # - port
-- -l   - logging

local headers = "HTTP/1.1 200 OK\r\nContent-Type: text/html; charset=UTF-8\r\nContent-Length: %d\r\n\r\n%s"
local head_html_Content = assert(io.open(rootFolder .. "header.html"))
local head = head_html_Content:read'*a'
local index_html_Content = assert(io.open(rootFolder .. serverIndexFilename))
local index = index_html_Content:read'*a'
local content = head .. index
local server = assert(socket.bind(serverHostname, serverPortNumber))

Verbose("Server started. http://" .. serverHostname .. ":" .. serverPortNumber,LOGGING_INFO)

head_html_Content:close()
index_html_Content:close()
repeat
   local client = server:accept()
   Verbose("-> Incomming connection",LOGGING_INFO)
	if(isParamDebug) then
		print(client)
		print("client:getpeername(): " .. client:getpeername())
		print("client:getstats(): " .. client:getstats())
		print("client:getfd(): " .. client:getfd())
	end
	client:settimeout(0.1)

    GETPath = {}
    GETPath[1] = tostring(client:receive('*l'))
    local _ = string.gsub(GETPath[1],"GET ","")
    GETPath[2] = string.gsub(_," HTTP/1.1","") _ = nil
    GETPath[3] = string.gsub(GETPath[2],"/","")
	local ok
	Verbose("HTTP request: " .. GETPath[1],LOGGING_INFO)
	Verbose("Requested file: " .. GETPath[2],LOGGING_INFO)
	if(GETPath[2] == "/") then
		ok = client:send(string.format(headers, #content, content))
		Verbose("-> File is root\n\t->Sending index",LOGGING_INFO)
	else
		local client_send_file = io.open(rootFolder..GETPath[3])
		if(client_send_file ~= nil) then
			local client_send_file_data = client_send_file:read'*a'
			io.close(client_send_file)
			if(GETPath[3]:match(".html")) then
				local interpret_content = Interpret(client_send_file_data)
				ok = client:send(string.format(headers,#interpret_content,interpret_content))
			else
				ok = client:send(string.format(headers,#client_send_file_data,client_send_file_data))
			end
			Verbose("-> Sending file "..rootFolder..GETPath[3],LOGGING_INFO)
		else
			local client_send_file = io.open(rootFolder.."404.html")
			local client_send_file_data = client_send_file:read'*a'
			io.close(client_send_file)
			ok = client:send(string.format(headers,#client_send_file_data,client_send_file_data))
			Verbose("-> File not found\t->Sending 404.html",LOGGING_WARNING)
		end
	end
	GETPath = nil
  Verbose("END.",LOGGING_INFO)
   client:close()
until not client or not ok
server:close()
